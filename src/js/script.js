var ready = function ( fn ) {
  // Sanity check
  if ( typeof fn !== 'function' ) return;

  // If document is already loaded, run method
  if ( document.readyState === 'interactive' || document.readyState === 'complete' ) {
    return fn();
  }

  // Otherwise, wait until document is loaded
  document.addEventListener( 'DOMContentLoaded', fn, false );
};

ready(function () {

  var rellax = new Rellax('.s-rellax');

  var newsSlider = new Swiper('.s-news__slider .swiper-container', {
    spaceBetween: 1,
    navigation: {
      nextEl: '.s-news__slider-nav-btn--next',
      prevEl: '.s-news__slider-nav-btn--prev',
    },
    breakpoints: {
      320: {
        slidesPerView: 1
      },
      576: {
        slidesPerView: 2
      }
    }
  });

  var gallerySlider = new Swiper('.s-gallery__slider .swiper-container', {
    slidesPerView: 1,
    spaceBetween: 1,
    navigation: {
      nextEl: '.s-gallery__slider-nav-btn--next',
      prevEl: '.s-gallery__slider-nav-btn--prev',
    }
  });

  var burger = document.querySelector('.s-page-header__hamburger');
  var mobileMenu = document.querySelector('.s-mobile-menu');

  if (burger) {
    burger.addEventListener('click', function () {
      if (mobileMenu) {
        if (mobileMenu.classList.contains('s-mobile-menu--is-active')) {
          mobileMenu.classList.remove('s-mobile-menu--is-active');
        } else {
          mobileMenu.classList.add('s-mobile-menu--is-active');
        }
      }
    });
  }

});